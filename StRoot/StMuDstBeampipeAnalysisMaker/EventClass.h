#ifndef EVENTCLASS_H
#define EVENTCLASS_H

#include <TROOT.h>
#include "Riostream.h"
#include "TClonesArray.h"
#include "TrackClass.h"

//Event Class Definition                                                        
class EVENT:public TObject{

 private:
  Long64_t runNumber;
  Long64_t eventNumber;
  Int_t nPions;
  Int_t trigID3;
  Int_t nGlobalTracks;
  Int_t nPrimaryTracks;
  Int_t refMult;
  Double_t vpdVz;
  Double_t xVertex;
  Double_t yVertex;
  Double_t zVertex;
  Double_t tpcDirection;
  Double_t vpdDiff;
  Double_t bbcZVertex;
  Double_t totalXMomentum;
  Double_t totalYMomentum;
  Double_t totalZMomentum;
  Double_t eventTime;
  Double_t productionTime;
  Double_t centerOfMassEnergy;
  Double_t initialBeamIntensity;
  Double_t zdcCoincidenceRate;
  Double_t bbcCoincidenceRate;
  Double_t backgroundRate;
  Double_t bbcBlueBackgroundRate;
  Double_t bbcYellowBackgroundRate;
  Double_t beamLifeTime;
  Double_t refMultPos; //
  Double_t refMultNeg; //
  Double_t bTOFTrayMultiplicity;//
  Int_t    nVerticies;//
  Int_t    nPileupVerticies;//

  TClonesArray *TrackClonesArray;

 public:
  EVENT();
  EVENT(Float_t *eventarray);
  virtual ~EVENT();

  void SetEventAttributes(Float_t *eventarray);
  void PrintEvent(Bool_t printtrack);
  void ResetEvent();
  //void AddTrackArray(TClonesArray *trackarray);
  void AddTrack(Float_t *trackarray, Int_t At);

  //Gets                                                                        
  Long64_t GetRunNumber(){return runNumber;}
  Long64_t GetEventNumber(){return eventNumber;}
  Int_t    GetNPions(){return nPions;}
  Int_t    GetTrigID3(){return trigID3;}
  Int_t    GetNGlobalTracks(){return nGlobalTracks;}
  Int_t    GetNPrimaryTracks(){return nPrimaryTracks;}
  Int_t    GetRefMult(){return refMult;}
  Int_t    GetRefMultPos(){return refMultPos;}
  Int_t    GetRegMultNeg(){return refMultNeg;}
  Int_t    GetnVerticies(){return nVerticies;}
  Int_t    GetnPileupVerticies(){return nPileupVerticies;}

  Double_t GetVPDVz(){return vpdVz;}
  Double_t GetXVertex(){return xVertex;}
  Double_t GetYVertex(){return yVertex;}
  Double_t GetZVertex(){return zVertex;}
  Double_t GetTPCDirection(){return tpcDirection;}
  Double_t GetVPDDiff(){return vpdDiff;}
  Double_t GetBBCZVertex(){return bbcZVertex;}
  Double_t GetTotalXMomentum(){return totalXMomentum;}
  Double_t GetTotalYMomentum(){return totalYMomentum;}
  Double_t GetTotalZMomentum(){return totalZMomentum;}
  Double_t GetEventTime(){return eventTime;}
  Double_t GetProductionTime(){return productionTime;}
  Double_t GetCenterOfMassEnergy(){return centerOfMassEnergy;}
  Double_t GetInitialBeamIntensity(){return initialBeamIntensity;}
  Double_t GetZDCCoincidenceRate(){return zdcCoincidenceRate;}
  Double_t GetBBCCoincidenceRate(){return bbcCoincidenceRate;}
  Double_t GetBackgroundRate(){return backgroundRate;}
  Double_t GetBBCBlueBackgroundRate(){return bbcBlueBackgroundRate;}
  Double_t GetBBCYellowBackgroundRate(){return bbcYellowBackgroundRate;}
  Double_t GetBeamLifeTime(){return beamLifeTime;}
  Double_t GetbTOFTrayMultiplicity(){return bTOFTrayMultiplicity;}

  //TRACK *GetTrack(Int_t i);
  Int_t GetTrackArrayEntries();

  ClassDef(EVENT,1);

};

#endif
