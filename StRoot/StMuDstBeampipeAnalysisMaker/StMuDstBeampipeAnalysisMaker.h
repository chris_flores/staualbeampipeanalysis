//                                                                                                             
// $Id: StMuDstBeampipeAnalysisMaker.h  $                                                                      
//                                                                                                             
#ifndef STAR_St_MuDstBeampipeAnalysis_Maker
#define STAR_St_MuDstBeampipeAnalysis_Maker

#ifndef StMaker_H
#include "StMaker.h"
#endif
class TH1D;
class TH2D;

class TH1F;
class TH2F;
class TNtuple;
class TTree;
class TBranch;
class EVENT;
class TRACK;
class TClonesArray;

class StMuDstBeampipeAnalysisMaker : public StMaker {

 public:
  StMuDstBeampipeAnalysisMaker(const char *name="StMuDstBeampipeAnalysis");
  ~StMuDstBeampipeAnalysisMaker();
  Int_t  Init();
  virtual void Clear(const char* opt="");
  Int_t  Make();
  Int_t  Finish();
  void  SetFileBaseName(const char* val) {fileBaseName = val; }
  void  SetFileIndex(const char* val) { mFileIndex = val; }
  void  SetOutDir(const char* val) { mOutDir = val; }
  virtual const char *GetCVS() const
  {   
            static const char cvs[]= "Tag $Name:  $ $Id: StMuDstBeampipeAnalysisMaker.h $ built __DATE__ __TIME__" ; return cvs;}
 private:
  TFile* dummyFile;
  int GoodEvent;

  TH2F* vertxy;
  TH2F* vertxygt;
  TH1F* vertz;
  TH1F* vertzgt;
  TH1F* vertx;
  TH1F* vertxgt;
  TH1F* verty;
  TH1F* vertygt;
  TH1F* vertr;
  TH1F* vertrgt;

  TBranch *eventBranch;

  float *EvValues;
  float *TrValues;

  TTree *outTree;
  EVENT *event;
  const char* fileBaseName;
  const char* mFileIndex;
  const char* mOutDir;
  ClassDef(StMuDstBeampipeAnalysisMaker, 1)
    };

#endif
