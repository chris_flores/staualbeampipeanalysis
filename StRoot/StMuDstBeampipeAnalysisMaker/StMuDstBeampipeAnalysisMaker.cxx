//This Maker 
//Author: Christopher E. Flores

//                                                                                                             
// $Id: StMuDstBeampipeAnalysisMaker.cxx $                                                                     
//                                                                                                             
#include <iostream>
#include "StMuDstBeampipeAnalysisMaker.h"

#include "TObject.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TNtuple.h"
#include "StLorentzVectorF.hh"
#include "StThreeVectorF.hh"
#include "StParticleTypes.hh"
#include "TMath.h"
#include "TTree.h"
#include "TrackClass.h"
#include "EventClass.h"
#include "TClonesArray.h"
#include <exception>

#include "StMuDSTMaker/COMMON/StMuDstMaker.h"
#include "StMuDSTMaker/COMMON/StMuDst.h"
#include "StMuDSTMaker/COMMON/StMuEvent.h"
#include "StMuDSTMaker/COMMON/StMuTrack.h"
//#include "StMuDSTMaker/Common/StEventInfo.h"                                                                 
#include "StTriggerIdCollection.h"
#include "StEvent/StBTofHeader.h" //necessary for checking the VPD minBias vertex                              
//#include "StMuBTofPidTraits.h"                                                                               

//TOF INFO COMES FROM GLOBAL TRACKS NOT PRIMARY TRACKS                                                         

#ifndef ST_NO_NAMESPACES
using std::vector;
#endif


static const char rcsid[] = "$Id: StMuDstBeampipeAnalysisMaker.cxx,v $";
ClassImp(StMuDstBeampipeAnalysisMaker)

//___________________________________________________________________________
StMuDstBeampipeAnalysisMaker::StMuDstBeampipeAnalysisMaker(const char *name): StMaker(name)
{
  if (Debug()) {
    cout << "StMuDstBeampipeAnalysisMaker::StMuDstBeampipeAnalysisMaker()" << endl;
  }
}
StMuDstBeampipeAnalysisMaker::~StMuDstBeampipeAnalysisMaker() { /* nopt */ }

//______________________________________________________________________________
Int_t StMuDstBeampipeAnalysisMaker::Init()
{
  cout << "StMuDstBeampipeAnalysisMaker::Init() - create histograms" << endl;


  //TString fileName("AuAl_3_0");
  TString fileName(fileBaseName);
  fileName.Append(mFileIndex);
  fileName.Append(".root");
  fileName.Prepend(mOutDir);

  // Display the resulting filename to the screen or log file             
  // (useful for debugging)
  cout << "Writing " << fileName << endl;

  // Create the ROOT file
  dummyFile = new TFile(fileName.Data(),"RECREATE");
  
  //Create an Event
  event = new EVENT();

  //Create the Output Tree
  outTree = new TTree("Events","Events");
  eventBranch = outTree->Branch("EventList",&event,1000000);

  //Create Histograms to be filled for vertex information                
  cout <<"=== Creating Histograms. ===" <<endl;
  vertxy = new TH2F("vertxy","xy position of primary vertex, before cuts",50,-5,5,50,-5,5);       //Filled before any cuts for all events
  vertxygt = new TH2F("vertxygt","xy position of primary vertex with all cuts",50,-5,5,50,-5,5);  //Filled with all cuts    
  vertz = new TH1F("vertz","z position of primary vertex, before cuts",100,-200,200);             //Filled before any cuts for all events
  vertzgt = new TH1F("vertzgt","z position of primary vertex with all cuts",100,-200,200);        //Filled with all cuts
  vertx = new TH1F("vertx","x position of primary vertex, before cuts",100,-5,5);                 //Filled before any cuts for all events
  vertxgt = new TH1F("vertxgt","x position of primary vertx with all cuts",100,-5,5);             //Filled with all cuts    
  verty = new TH1F("verty","y position of primary vertex, before cuts",100,-5,5);                 //Filled before any cuts for all events
  vertygt = new TH1F("vertygt","y position of primary vertx with all cuts",100,-5,5);             //Filled with all cuts    
  vertr = new TH1F("vertr","r position of the primary vertex, before cuts",50,0,5);               //Filled before any cuts for all events
  vertrgt = new TH1F("vertrgt","r position of the primary vertex with all cuts",50,0,5);          //Filled with all cuts    
  GoodEvent = 0;

  cout << "StMuDstBeampipeAnalysisMaker::Init() - successful" << endl;

  //Array to hold the event and track info
  //These get reset to default values each time Make() is called
  EvValues = new float[32];
  TrValues = new float[49];
  

  return StMaker::Init();
}

//_____________________________________________________________________________
void StMuDstBeampipeAnalysisMaker::Clear(const char* c){

  //Reset the Event
  event->ResetEvent();
  cout <<"ResetEvent" <<endl;


  cout <<"Clear Successful" <<endl;
  return StMaker::Clear(c);
}

//_____________________________________________________________________________
Int_t StMuDstBeampipeAnalysisMaker::Make(){

  cout <<"Starting Make()" <<endl;

  const double pmass = StProton::instance()->mass();
  const double pimass = StPionPlus::instance()->mass();
  const double kmass = StKaonPlus::instance()->mass();
  const double dmass = StDeuteron::instance()->mass();
  const double tmass = StTriton::instance()->mass();
  const double Hemass = StHe3::instance()->mass();

  StMuDstMaker* muDstMaker = (StMuDstMaker*) GetMaker("MuDst");
  if (!muDstMaker) {
    cout << "Can't get muDstMaker!!! bail out..." << endl;
    return kStWarn;
  }

  StMuDst* mMuDst = muDstMaker->muDst();
  StMuEvent* mEvent = mMuDst->event();

  size_t Nprimary = mMuDst->primaryTracks()->GetEntries();

  if (Debug()>0) {
    cout << "Num. of primary tracks: " << Nprimary << endl;
  }
  if (Nprimary==0) {
    if (Debug()>0) {
      cout << "Vertex was not found!" << endl;
    }
    return kStOk;
  }
  if (Nprimary < 2){
    if (Debug()>0) {
      cout<<"Number of particles out of range, multiplicity = "<<Nprimary<<endl;
      cout << "Event ID: " << mEvent->eventId() << endl;
    }
    return kStOk;
  }

  //Get the Primary Vertex Position
  StThreeVectorF prim = mEvent->primaryVertexPosition();
  float x = prim.x();
  float y = prim.y();
  float z = prim.z();
  float r = pow((x*x + y*y),0.5);

  //Fill the vertex histograms that don't have cuts
  vertxy->Fill(x,y);
  vertz->Fill(z);
  vertx->Fill(x);
  verty->Fill(y);
  vertr->Fill(r);


  //Initialize the Event Level values to some unreasonable, default number
  for (int g = 0; g < 32; g++){ EvValues[g] = -2000; }

  /********EVENT CUTS ********/

  //Radial Cut                                                                                                
  if (r < 2.0 || r > 5.0){
    if (Debug()>0) {
      cout<<"r vertex out of range r = "<<r<<endl;
      cout << "Vertex Position : " << prim << endl;
    }
    return kStOk;
  }
  //Z Vertex Cut                                                                                   
  if (z>200.0 || z<-200.0 || (z<150.0 && z>-150.0) ){
    if (Debug()>0) {
      cout<<"z vertex out of range z = "<<r<<endl;
      cout << "Vertex Position : " << prim << endl;
    }
    return kStOk;
  }

  //Total Momentum for a give track
  StThreeVectorF TotalMom(0,0,0);

  //Initialize the Track variables to some unreasonable, default number
  for(int i=0; i<49; i++){
    TrValues[i]=-9999;
  }

  //Number of Pions in the Event
  int NumPions = 0;
  
  //Loop Over all the primary Tracks in the event
  //Apply some basic track QA Cuts and count the number
  //of pion in the event
  for (size_t i=0; i < Nprimary; ++i) {
    
    StMuTrack* prTrack = muDstMaker->muDst()->primaryTracks(i);
    
    if (Debug()>2) {
      cout << "Track " << i << " has nHitsFit/nHitsPoss = "
           << prTrack->nHitsFit()/static_cast<float>(prTrack->nHitsPoss())
           << ", nHitsFit= " << prTrack->nHitsFit() << ", nHitsPoss= "
           << prTrack->nHitsPoss() << endl;
    }
    if ((prTrack->nHitsFit()/static_cast<float>(prTrack->nHitsPoss())) < 0.50) { continue; } // Track quality cut            
    if (prTrack->flag()>=1000) { continue; } //another track quality cut                                                     
    
    StThreeVectorF mom = prTrack->momentum();
    TotalMom = TotalMom + mom;
    if (Debug()>2) {
      cout << "Track " << i << " has p = " << mom << endl;
    }
    if ( prTrack->nSigmaPion() < 2.0 && prTrack->nSigmaPion() > -2.0 ) {
      if( prTrack->charge()>0 ){
        if (prTrack->nSigmaProton() < -1.0 ) { NumPions += 1; }
      }
      else { NumPions += 1; }
    }
  }//End Loop Over the Primary Tracks of the event
  
  cout <<"Counted Pions. There are: " <<NumPions <<endl;
  
  //This variable makes sure that the event is headed
  //into the TPC rather than away from it. This is also
  //the last event cut so if the event passes then fill the
  //"Good" Event histograms with the vetex info
  float TPCDirection = TotalMom.z()*z;

  if (TPCDirection > 0){
    if (Debug()>0) {
      cout<<"Event leaves TPC"<<endl;
    }
    return kStOk;
  }
  else {
    vertxygt->Fill(x,y);
    vertzgt->Fill(z);
    vertxgt->Fill(x);
    vertygt->Fill(y);
    vertrgt->Fill(r);
  }

  /*************** End of Event and Track Cuts/ Start filling data structures ************/

  //Fill The Event Values
  EvValues[0]   = mEvent->runId();
  EvValues[1]   = mEvent->eventId();
  EvValues[2]   = NumPions;
  EvValues[3]   = 0;
  if ((float)mEvent->triggerIdCollection().nominal().isTrigger(12)){ //change trigger number to mb-slow                    
    EvValues[3] = 1.;
  }
  EvValues[4]   = mMuDst->globalTracks()->GetEntries();
  EvValues[5]   = Nprimary;
  EvValues[6]   = mEvent->refMult();
  EvValues[7]   = mEvent->vpdVz();
  EvValues[8]   = x;
  EvValues[9]   = y;
  EvValues[10]  = z;
  EvValues[11]  = TPCDirection;
  EvValues[12]  = mEvent->vpdTdiff();
  EvValues[13]  = mEvent->bbcTriggerDetector().zVertex();
  EvValues[14]  = TotalMom.x();
  EvValues[15]  = TotalMom.y();
  EvValues[16]  = TotalMom.z();
  EvValues[17]  = mEvent->eventInfo().time();
  EvValues[18]  = mEvent->runInfo().productionTime();
  EvValues[19]  = mEvent->runInfo().centerOfMassEnergy();
  EvValues[20]  = 0; //mEvent->runInfo().initialBeamIntensity();
  EvValues[21]  = mEvent->runInfo().zdcCoincidenceRate();
  EvValues[22]  = mEvent->runInfo().bbcCoincidenceRate();
  EvValues[23]  = mEvent->runInfo().backgroundRate();
  EvValues[24]  = mEvent->runInfo().bbcBlueBackgroundRate();
  EvValues[25]  = mEvent->runInfo().bbcYellowBackgroundRate();
  EvValues[26]  = 0; //mEvent->runInfo().beamLifeTime();
  EvValues[27]  = mEvent->refMultPos();
  EvValues[28]  = mEvent->refMultNeg();
  EvValues[29]  = mEvent->btofTrayMultiplicity();
  EvValues[30]  = mEvent->eventSummary().numberOfVertices();
  EvValues[31]  = mEvent->eventSummary().numberOfPileupVertices();

 
  //Set the Event Attributes
  event->SetEventAttributes(EvValues);

  //Now Loop Over all the Primary Tracks of the Event
  int nAddedTracks = 0;
  for (size_t i=0; i < Nprimary; ++i){
    if (i==0){
      GoodEvent++;
      if (Debug()>0 ) {
	cout<<"# of Good Events = "<<GoodEvent<<endl;
      }
    }

    //Get the Primary Track and its basic info
    StMuTrack* prTrack = muDstMaker->muDst()->primaryTracks(i);
    StThreeVectorF momentum = prTrack->momentum();
    StThreeVectorF firstPoint = prTrack->firstPoint();
    StThreeVectorF lastPoint = prTrack->lastPoint();
    StThreeVectorF globalDCA = prTrack->dcaGlobal();
    StThreeVectorF primaryDCA = prTrack->dca();

    //Apply basic track cuts
    if ((prTrack->nHitsFit()/static_cast<float>(prTrack->nHitsPoss())) < 0.5) { continue; } // Track quality cut
    if (prTrack->flag()>999 || prTrack->flag()<0) { continue; } //another track quality cut 
  
    //Set the Mass Assumption for the track assuming each particle mass
    StLorentzVectorF pitrack(prTrack->momentum(),prTrack->momentum().massHypothesis(pimass));
    StLorentzVectorF ptrack(prTrack->momentum(),prTrack->momentum().massHypothesis(pmass));
    StLorentzVectorF ktrack(prTrack->momentum(),prTrack->momentum().massHypothesis(kmass));
    StLorentzVectorF dtrack(prTrack->momentum(),prTrack->momentum().massHypothesis(dmass));
    StLorentzVectorF Ttrack(prTrack->momentum(),prTrack->momentum().massHypothesis(tmass));
    StLorentzVectorF He3track(prTrack->momentum(),prTrack->momentum().massHypothesis(Hemass));

    //Compute the rapidity of the track assumming different masses
    float ypi = pitrack.rapidity();
    float yp = ptrack.rapidity();
    float yk = ktrack.rapidity();
    float yd = dtrack.rapidity();
    float yt = Ttrack.rapidity();
    float yHe = He3track.rapidity();

    const StMuBTofPidTraits& tofTraits = prTrack->btofPidTraits();

    //Fill all the Track Level Values
    TrValues[0]  = (float)prTrack->nHitsFit();
    TrValues[1]  = (float)prTrack->nHitsPoss();
    TrValues[2]  = (float)prTrack->flag();
    TrValues[3]  = momentum.z();
    TrValues[4]  = prTrack->pt();
    TrValues[5]  = prim.x();
    TrValues[6]  = prim.y();
    TrValues[7]  = prim.z();
    TrValues[8]  = prTrack->dEdx();
    TrValues[9]  = prTrack->charge();
    TrValues[10] = tofTraits.timeOfFlight();
    TrValues[11] = tofTraits.beta();
    TrValues[12] = prTrack->eta();
    TrValues[13] = prTrack->phi();
    TrValues[14] = prTrack->nSigmaElectron();
    TrValues[15] = prTrack->nSigmaPion();
    TrValues[16] = prTrack->nSigmaKaon();
    TrValues[17] = prTrack->nSigmaProton();
    TrValues[18] = prTrack->pidProbElectron();
    TrValues[19] = prTrack->pidProbPion();
    TrValues[20] = prTrack->pidProbKaon();
    TrValues[21] = prTrack->pidProbProton();
    TrValues[22] = ypi;
    TrValues[23] = yk;
    TrValues[24] = yp;
    TrValues[25] = yd;
    TrValues[26] = yt;
    TrValues[27] = yHe;
    TrValues[28] = prTrack->dcaD();
    TrValues[29] = prTrack->dcaZ();
    TrValues[30] = (float)prTrack->nHits();
    TrValues[31] = (float)prTrack->nHitsDedx();
    TrValues[32] = firstPoint.z();
    TrValues[33] = lastPoint.z();
    TrValues[34] = tofTraits.sigmaElectron();
    TrValues[35] = tofTraits.sigmaPion();
    TrValues[36] = tofTraits.sigmaKaon();
    TrValues[37] = tofTraits.sigmaProton();
    TrValues[38] = tofTraits.probElectron();
    TrValues[39] = tofTraits.probPion();
    TrValues[40] = tofTraits.probKaon();
    TrValues[41] = tofTraits.probProton();
    TrValues[42] = tofTraits.pathLength();
    TrValues[43] = globalDCA.x();
    TrValues[44] = globalDCA.y();
    TrValues[45] = globalDCA.z();
    TrValues[46] = primaryDCA.x();
    TrValues[47] = primaryDCA.y();
    TrValues[48] = primaryDCA.z();


    //Add The Track to The Event
    event->AddTrack(TrValues,nAddedTracks);
    nAddedTracks++;

  }//End Track Loop

  //event->PrintEvent(true);
  
  //Fill the Tree With the Event
  outTree->Fill();
  cout <<"Filled The Tree" <<endl;

  return kStOK;
}

//_____________________________________________________________________________
Int_t StMuDstBeampipeAnalysisMaker::Finish()
{
  if (Debug()>0) {
    cout << "Total Good Events seen : " << GoodEvent << endl;
  }
  // Write out the histograms to a file.                
  // Here, the filename is hardwired, except for one string that is given in the macro.
  dummyFile->Write();
  dummyFile->Close();

  return kStOK;
}
