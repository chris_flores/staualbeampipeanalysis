This is the code base responsbile for colleting the data from the muDSTs on RCF
for the mid rapidity Coulomb analysis (PSN6042) and the other halo on beampipe
analysis.

Author: Christopher E. Flores
Contributors: Samantha Brovko, Brooke Haag, Daniel Cebra
Data: June 2012
Last Updated: November 2015

OVERVIEW:
	The purpose of the code is to run over the MuDst files stored on RCF and extract
	the event and track information for events between beam halo nuclei (Au) and the
	Aluminum beam pipe. The data are sored in a tree containing an event class which
	itself, contains track-by-track information in an array of track objects using
	a track class. 	The resulting data files cab be processed using the AuAlBeampipe 
	analysis codebase.

CONTENTS:
	This directory should contain the following:
	     README             - This file
	     StRoot/            - contains the data collection maker and class files
	     RunMuDstBeampipe.C - Macro which calls the data colletion maker
	     AuAl_3_0/          - contains code for running over the 3.0 GeV Data
	     AuAl_3_5/          - contains code for running over the 3.5 GeV Data
	     AuAl_4_5/          - contains code for running over the 4.5 GeV Data
	     
HOW TO USE:
    First, create a directory structure for the output data files and log files. Choose
    a directory for which you have write access and which has sufficient disk space.
    Here is a suggestion that can be used exactly for the Mid-Rapidity Coulomb analysis (PSN0642):
    	 /star/data05/scratch/<yourUserName>/BeamPipeAnalysis/
		|-> AuAl_3_0/ 
		    |-> LogFiles/
		    |-> Sched/
		|-> AuAl_3_5/
		    |-> LogFiles/
		    |-> Sched/
		|-> AuAl_4_5/
                    |-> LogFiles/
		    |-> Sched/

    To run this data collection maker:
       1. Change directory into the energy you would like collect data for.
       2. Edit the xml file:
       	       - Change the path for the stdout, stderr, Generator, and output tags to the 
	         location you decided on above.
	       - Change the path to the macro and StRoot file in the SandBox tag as well.
       3. Run the data collection maker for this energy in simulation mode (Default) by doing:
       	       star-submit <nameOfXMLFile>.xml 
       4. Assuming the simulation is successful run the jobs:
               star-submit -r all <theGeneratedSchedulerFile>.xml
    
    Repeat these steps for other energies if you desire.